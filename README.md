This is the Crossref Assets CDN repository. You should not normally add files here. All files put here must remain unchanged for perpetuity as they are relied on by third parties.

# Automation

This is run via GitLab CI automatically whenever the master branch is updated. The following config variables are expected and should be set in the CI settings for the project.

 - DIST_ID - the CloudFront distribution ID
 - S3_BUCKET - the S3 bucket where data is stored
 - AWS_ACCESS_KEY_ID - AWS access key with permission to uplaod to S3 bc	
 - AWS_SECRET_ACCESS_KEY
