All of these files have been referenced at some point so must remain here, even if not referenced by the current index page.

The following are stored here but no longer linked officially:

 - crossref-logo-100.png
 - crossref-logo-100.svg
 - crossref-logo-100@2x.png
 - crossref-logo-128.png
 - crossref-logo-200.png
 - crossref-logo-200@2x.png
 - crossref-logo-landscape-100.png
 - crossref-logo-landscape-100@2x.png
 - crossref-logo-landscape-200.png
 - crossref-logo-landscape-200@2x.png
 - crossref-logo-stacked-200.svg